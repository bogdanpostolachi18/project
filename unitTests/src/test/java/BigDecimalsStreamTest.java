import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import tests.BigDecimalsStream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class BigDecimalsStreamTest {

    @Test
    public void computeSumTest() {
        List<BigDecimal> all = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            all.add(new BigDecimal(2*i));
        }
        BigDecimalsStream stream = new BigDecimalsStream(all);
        BigDecimal sum = stream.computeSum();
        BigDecimal res = new BigDecimal(420);
        Assertions.assertEquals(res,sum);
    }

    @Test
    public void computeAverageTest() {
        List<BigDecimal> all = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            all.add(new BigDecimal(2*i));
        }
        BigDecimalsStream stream = new BigDecimalsStream(all);
        BigDecimal avg = stream.computeAverage();
        BigDecimal res = new BigDecimal(21);
        Assertions.assertEquals(res,avg);
    }

    @Test
    public void computeTop10Test() {
        List<BigDecimal> all = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            all.add(new BigDecimal(2*i));
        }
        BigDecimalsStream stream = new BigDecimalsStream(all);
        Stream<BigDecimal> top10stream = stream.computeTop10();
        List<BigDecimal> res = new ArrayList<>();
        top10stream.forEach(res::add);

        List<BigDecimal> arr = new ArrayList<>();
        for (int i = 11; i <= 20; i++)
            arr.add(new BigDecimal(i));
        Assertions.assertEquals(res,arr);
    }
}
