package tests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class BigDecimalsStream {
    private final List<BigDecimal> values;

    public BigDecimalsStream() {
        this.values = new ArrayList<>();
    }

    public BigDecimalsStream(List<BigDecimal> list) {
        this.values = list;
    }

    public BigDecimal computeSum() {
        Stream<BigDecimal> stream = this.values.stream();
        return stream.reduce(BigDecimal.ZERO,BigDecimal::add);
    }

    public BigDecimal computeAverage() {
        Stream<BigDecimal> stream = this.values.stream();
        BigDecimal sum = stream.reduce(BigDecimal.ZERO,BigDecimal::add);
        return sum.divide(new BigDecimal(this.values.size()));
    }

    public Stream computeTop10() {
        Stream<BigDecimal> stream = this.values.stream();
        Stream<BigDecimal> sorted = stream.sorted();
        return sorted.limit(this.values.size()/10);
    }
}