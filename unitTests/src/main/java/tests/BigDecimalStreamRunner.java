package tests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class BigDecimalStreamRunner {
    private static ArrayList<BigDecimal> getValues() {
        List<BigDecimal> all = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            all.add(new BigDecimal(2*i));
        }
        return (ArrayList<BigDecimal>) all;
    }

    private static final List<BigDecimal> values = getValues();

    private static final BigDecimalsStream stream = new BigDecimalsStream(values);

    public static void main(String[] args) {
        computeSumTest();
        computeAverageTest();
        computeTop10Test();
    }

    public static void computeSumTest() {
        BigDecimal sum = stream.computeSum();
        BigDecimal res = new BigDecimal(420);
        System.out.println(sum);
    }

    public static void computeAverageTest() {
        BigDecimal avg = stream.computeAverage();
        BigDecimal res = new BigDecimal(21);
        System.out.println(avg);
    }

    public static void computeTop10Test() {
        Stream<BigDecimal> top10stream = stream.computeTop10();
        top10stream.forEach(System.out::println);
    }
}
