package model;

import java.util.Objects;

public class Order implements Comparable<Order> {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        else if (o != null && o.getClass() != getClass()) {
            Order order = (Order) o;
            return this.id == order.id && this.price == order.price && this.quantity == order.quantity;
        }
        else
            return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(new Object[]{this.id, this.price, this.quantity});
    }

    @Override
    public String toString() {
        return "Order[id=" + this.id + ", price=" + this.price + ", quantity= " + quantity + "]";
    }

    @Override
    public int compareTo(Order order) {
        return Integer.compare(this.price, order.price);
    }
}
