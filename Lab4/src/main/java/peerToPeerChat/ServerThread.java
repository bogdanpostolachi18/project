package peerToPeerChat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ServerThread extends Thread {
    private final ServerSocket serverSocket;
    private final Set<ServerThreadThread> serverThreadThreads = new HashSet<>();
    public ServerThread(String port) throws IOException {
        this.serverSocket = new ServerSocket(Integer.parseInt(port));
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket socket = this.serverSocket.accept();
                ServerThreadThread serverThreadThread =
                        new ServerThreadThread(socket,this);
                this.serverThreadThreads.add(serverThreadThread);
                serverThreadThread.start();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        try {
            serverThreadThreads.forEach(t -> t.getPrintWriter().println(message));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Set<ServerThreadThread> getServerThreadThreads() {
        return this.serverThreadThreads;
    }
}
