package peerToPeerChat;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;

public class Peer {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter username and port for this peer (space separated): ");
        String[] setupValues = reader.readLine().split(" ");
        ServerThread serverThread = new ServerThread(setupValues[1]);
        serverThread.start();
        new Peer().updateListenToPeers(reader,setupValues[0],serverThread);
    }

    public void updateListenToPeers(BufferedReader reader,
                                    String username,
                                    ServerThread serverThread) throws Exception {
        System.out.println("Enter (space separated) hostname:port#");
        System.out.println("  peers to receive messages from (s to skip):");
        String input = reader.readLine();
        String[] inputValues = input.split(" ");
        if (!input.equals("s")) {
            for (String inputValue : inputValues) {
                String[] address = inputValue.split(":");
                Socket socket = null;
                try {
                    socket = new Socket(address[0], Integer.parseInt(address[1]));
                    new PeerThread(socket).start();
                } catch (Exception ex) {
                    if (socket != null) {
                        socket.close();
                    } else {
                        System.out.println("Invalid input. Skipping to next step");
                    }
                }
            }
        }

        communicate(reader,username,serverThread);
    }

    public void communicate(BufferedReader reader, String username, ServerThread serverThread) {
        try {
            System.out.println("You can now communicate (type 'e' to exit or 'c' to change");
            boolean flag = true;
            while (flag) {
                String message = reader.readLine();
                if (message.equals("e")) {
                    flag = false;
                    break;
                }
                else if (message.equals("c")) {
                    updateListenToPeers(reader,username,serverThread);
                }
                else {
                    StringWriter sWriter = new StringWriter();
                    Json.createWriter(sWriter).writeObject(Json.createObjectBuilder()
                            .add("username",username)
                            .add("message",message)
                            .build());

                    serverThread.sendMessage(sWriter.toString());
                }
            }
            System.exit(0);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
