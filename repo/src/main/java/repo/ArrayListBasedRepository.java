package repo;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    public ArrayListBasedRepository(int capacity) {
        this.list = new ArrayList<>(capacity);
    }

    @Override
    public void add(T value) {
        this.list.add(value);
    }

    @Override
    public boolean contains(T value) {
        return this.list.contains(value);
    }

    @Override
    public void remove(T value) {
        this.list.remove(value);
    }

    public T get(int index) {
        return this.list.get(index);
    }
}
