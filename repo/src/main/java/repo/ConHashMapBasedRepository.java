package repo;

import java.util.concurrent.ConcurrentHashMap;

public class ConHashMapBasedRepository<K,V> implements MapInMemoryRepository<K,V> {
    private final ConcurrentHashMap<K,V> hashMap;

    public ConHashMapBasedRepository() {
        this.hashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(K key, V value) {
        this.hashMap.put(key,value);
    }

    @Override
    public boolean contains(K key) {
        return this.hashMap.contains(key);
    }

    @Override
    public void remove(K key) {
        this.hashMap.remove(key);
    }
}
