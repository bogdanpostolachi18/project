package repo;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public HashSetBasedRepository() {
        this.set = new HashSet<>();
    }

    @Override
    public void add(T value) {
        this.set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return this.set.contains(value);
    }

    @Override
    public void remove(T value) {
        this.set.remove(value);
    }
}
