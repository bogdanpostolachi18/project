package repo;

public interface MapInMemoryRepository<K,V> {
    void add(K key, V value);
    boolean contains(K key);
    void remove(K key);
}
