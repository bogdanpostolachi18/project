package repo;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSet<>();
    }

    @Override
    public void add(T value) {
        this.set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return this.set.contains(value);
    }

    @Override
    public void remove(T value) {
        this.set.remove(value);
    }
}
