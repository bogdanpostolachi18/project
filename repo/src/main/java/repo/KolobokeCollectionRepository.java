package repo;

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class KolobokeCollectionRepository<T> implements InMemoryRepository<T> {
    private final HashObjSet<T> set;

    public KolobokeCollectionRepository() {
        this.set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T value) {
        this.set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return this.set.contains(value);
    }

    @Override
    public void remove(T value) {
        this.set.remove(value);
    }
}
