package repo;

public interface InMemoryRepository<T> {
    void add(T value);
    boolean contains(T value);
    void remove(T value);

}
