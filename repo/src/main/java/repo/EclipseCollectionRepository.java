package repo;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class EclipseCollectionRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> list;

    public EclipseCollectionRepository() {
        this.list = new FastList<>();
    }

    public EclipseCollectionRepository(int capacity) {
        this.list = new FastList<>(capacity);
    }

    @Override
    public void add(T value) {
        this.list.add(value);
    }

    @Override
    public boolean contains(T value) {
        return this.list.contains(value);
    }

    @Override
    public void remove(T value) {
        this.list.remove(value);
    }

    public T get(int index) {
        return this.list.get(index);
    }
}
