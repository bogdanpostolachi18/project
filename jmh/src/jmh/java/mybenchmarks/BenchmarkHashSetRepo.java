package mybenchmarks;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repo.HashSetBasedRepository;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class BenchmarkHashSetRepo {
    @Param({"10000"})
    private int N;

    private final HashSetBasedRepository<Order> hashSetRepo = new HashSetBasedRepository<>();

    @Benchmark
    public void testAdd(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.hashSetRepo.add(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testContains(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.hashSetRepo.contains(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testRemove(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.hashSetRepo.remove(order);
            bh.consume(order);
        }
    }
}
