package mybenchmarks;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repo.ArrayListBasedRepository;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 2, jvmArgs = {"-Xms4G", "-Xmx4G"})
@State(Scope.Benchmark)
public class BenchmarkArrListRepo {
    @Param({"10000"})
    private int N;
    private final ArrayListBasedRepository<Order> orderRepo = new ArrayListBasedRepository<>(N);

    @Benchmark
    public void testAdd(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            orderRepo.add(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testContains(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            orderRepo.contains(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testRemove(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            orderRepo.remove(order);
            bh.consume(order);
        }
    }
}
