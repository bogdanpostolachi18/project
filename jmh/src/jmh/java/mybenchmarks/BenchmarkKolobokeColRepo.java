package mybenchmarks;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repo.KolobokeCollectionRepository;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class BenchmarkKolobokeColRepo {
    @Param({"10000"})
    private int N;

    private final KolobokeCollectionRepository<Order> kolRepo = new KolobokeCollectionRepository<>();

    @Benchmark
    public void testAdd(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.kolRepo.add(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testContains(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.kolRepo.contains(order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testRemove(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            this.kolRepo.remove(order);
            bh.consume(order);
        }
    }
}
