package mybenchmarks;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 2, jvmArgs = {"-Xms4G", "-Xmx4G"})
@State(Scope.Benchmark)
public class DoublePrimitiveStreams {
    @Param({"100000000"})
    private int N;

    @Benchmark
    public void computeSum(Blackhole consumer) {
        DoubleStream stream = DoubleStream.generate(()
                -> { return (double)(Math.random() * 10000); });
        consumer.consume(stream
                .limit(N)
                .sum());
    }

    @Benchmark
    public void computeAverage(Blackhole consumer) {
        DoubleStream stream = DoubleStream.generate(()
                -> { return (double)(Math.random() * 10000); });
        double s = stream
                .limit(N)
                .sum();
        consumer.consume(s/N);
    }

    @Benchmark
    public void computeTop10(Blackhole consumer) {
        DoubleStream stream = DoubleStream.generate(()
                -> { return (double)(Math.random() * 10000); });
        consumer.consume(stream
                .sorted()
                .limit((long) (N * 0.1)));
    }
}
