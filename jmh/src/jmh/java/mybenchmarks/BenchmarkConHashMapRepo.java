package mybenchmarks;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repo.ConHashMapBasedRepository;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class BenchmarkConHashMapRepo {
    @Param({"10000"})
    private int N;

    private final ConHashMapBasedRepository<String,Order> orderRepo = new ConHashMapBasedRepository<>();

    @Benchmark
    public void testAdd(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            Order order = new Order(i,i,100);
            orderRepo.add(String.valueOf(i),order);
            bh.consume(order);
        }
    }

    @Benchmark
    public void testContains(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            String key = String.valueOf(i);
            orderRepo.contains(key);
            bh.consume(key);
        }
    }

    @Benchmark
    public void testRemove(Blackhole bh) {
        for (int i = 0; i < N; i++) {
            String key = String.valueOf(i);
            orderRepo.contains(key);
            bh.consume(key);
        }
    }
}
