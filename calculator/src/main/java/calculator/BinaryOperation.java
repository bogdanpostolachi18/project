package calculator;

public interface BinaryOperation {
    double calculate(double op1, double op2);
}