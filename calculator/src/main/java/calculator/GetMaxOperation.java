package calculator;

public class GetMaxOperation implements BinaryOperation{
    private double firstOperand;

    private double secondOperand;

    public GetMaxOperation(){}

    public GetMaxOperation(double firstOperand, double secondOperand){
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    @Override
    public double calculate(double op1, double op2) {
        return Math.max(op1,op2);
    }

    public double getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(double firstOperand) {
        this.firstOperand = firstOperand;
    }

    public double getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(double secondOperand) {
        this.secondOperand = secondOperand;
    }
}
