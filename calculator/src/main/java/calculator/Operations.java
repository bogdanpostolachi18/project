package calculator;

public enum Operations {
    ADD,
    SUB,
    MUL,
    DIV,
    MIN,
    MAX,
    SQUARE_ROOT
}
