package calculator;

import java.util.Stack;

public class Calculator {
    private final AddOperation addOperation;
    private final SubOperation subOperation;
    private final MulOperation mulOperation;
    private final DivOperation divOperation;
    private final GetMinOperation getMinOperation;
    private final GetMaxOperation getMaxOperation;
    private final SquareRootOperation squareRootOperation;
    private Stack<Operations> operators;
    private Stack<Double> operands;

    public Calculator() {
        this.addOperation = new AddOperation();
        this.subOperation = new SubOperation();
        this.mulOperation = new MulOperation();
        this.divOperation = new DivOperation();
        this.getMinOperation = new GetMinOperation();
        this.getMaxOperation = new GetMaxOperation();
        this.squareRootOperation = new SquareRootOperation();
        this.operators = new Stack<>();
    }

    private boolean hasHigherPrecedence(Operations op1, String op2){
        return (op1 == Operations.MUL || op1 == Operations.DIV)
                && (op2.equals("+") || op2.equals("-"));
    }

    private String getOperator(String cmd){
        if (cmd.equals("+") || cmd.equals("-") || cmd.equals("*") || cmd.equals("/"))
            return cmd;
        return null;
    }

    private String enumToStringOp(Operations op){
        if (op == Operations.ADD)
            return "+";
        if (op == Operations.SUB)
            return "-";
        if (op == Operations.MUL)
            return "*";
        if (op == Operations.DIV)
            return "/";
        return " ";
    }

    private String parseCommand(String command){
        String[] tokens = command.split(" ");
        String postfix = "";
        for (String t : tokens){
            if (getOperator(t) != null){
                while (!operators.empty() && hasHigherPrecedence(operators.peek(),t)) {
                    postfix += enumToStringOp(operators.pop());
                    postfix += " ";
                }

                switch(t){
                    case "+":
                        operators.add(Operations.ADD);
                        break;
                    case "-":
                        operators.add(Operations.SUB);
                        break;
                    case "*":
                        operators.add(Operations.MUL);
                        break;
                    case "/":
                        operators.add(Operations.DIV);
                    default:
                        break;
                }
            }
            else {
                postfix += t;
                postfix += " ";
            }
            while (!operators.empty())
                postfix += enumToStringOp(operators.pop());
        }
        return postfix;
    }

    public double calculate(String command){
        String postfix = parseCommand(command);
        String[] tokens = postfix.split(" ");
        Double res = (double) 0;
        for (String t: tokens){
            if (getOperator(t) == null) {
                try {
                    Double number = Double.parseDouble(t);
                    operands.push(number);
                }
                catch(NumberFormatException ex){
                    System.out.println(ex);
                }
            }
            else{
                Double op1 = operands.pop();
                Double op2 = operands.pop();
            }
        }
        return 0d;
    }
}