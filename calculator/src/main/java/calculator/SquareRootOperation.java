package calculator;

public class SquareRootOperation {
    private double operand;

    public SquareRootOperation(){}

    public SquareRootOperation(double operand){
        this.operand = operand;
    }

    public double calculate(double operand){
        return Math.sqrt(operand);
    }

    public double getOperand() {
        return operand;
    }

    public void setOperand(double operand) {
        this.operand = operand;
    }
}
